cmake_minimum_required(VERSION 3.11)

project(eego-sdk-pybind11)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

include(user.cmake)
include(cmake/eego-sdk.cmake)
include(cmake/pybind11.cmake)

find_package(PythonLibs 2 REQUIRED)

add_library(eego_sdk MODULE eego_sdk.cc)
target_include_directories(eego_sdk SYSTEM PRIVATE ${EEGO_SDK_ROOT})
target_include_directories(eego_sdk SYSTEM PRIVATE ${PYTHON_INCLUDE_DIRS})
target_include_directories(eego_sdk SYSTEM PRIVATE ${PYBIND11_INCLUDE_DIR})
target_compile_definitions(eego_sdk PRIVATE EEGO_SDK_BIND_STATIC)
target_compile_definitions(eego_sdk PRIVATE _UNICODE)
target_link_libraries(eego_sdk PRIVATE ${EEGO_SDK_LIBRARY})
if(UNIX)
  set_target_properties(eego_sdk PROPERTIES PREFIX "")
endif()
if(WIN32)
  set_target_properties(eego_sdk PROPERTIES SUFFIX .pyd)
endif()
