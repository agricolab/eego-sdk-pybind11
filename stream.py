#!/usr/bin/python

import time

import eego_sdk
###############################################################################
def amplifier_to_id(amplifier):
  return '{}-{:06d}-{}'.format(amplifier.getType(), amplifier.getFirmwareVersion(), amplifier.getSerialNumber())
###############################################################################
def test_impedance(amplifier):
  stream = amplifier.OpenImpedanceStream()

  print('stream:')
  print('  channels.... {}'.format(stream.getChannelList()))
  print('  impedances.. {}'.format(list(stream.getData())))
###############################################################################
def test_eeg(amplifier):
  rates = amplifier.getSamplingRatesAvailable()
  ref_ranges = amplifier.getReferenceRangesAvailable()
  bip_ranges = amplifier.getBipolarRangesAvailable()
  stream = amplifier.OpenEegStream(rates[0], ref_ranges[0], bip_ranges[0])

  print('stream:')
  print('  channels:   {}'.format(stream.getChannelList()))

  stream_channel_count = len(stream.getChannelList())

  with open('%s-eeg.txt' % (amplifier_to_id(amplifier)), 'w') as eeg_file:
    # get data for 10 seconds, 0.25 seconds in between
    t0 = time.time()
    t1 = t0 + 10
    interval = 0.25
    tnext = t0
    while time.time() < t1:
      tnext = tnext + interval
      delay = tnext - time.time()
      if delay > 0:
        time.sleep(delay)

      data = stream.getData()
      print('  [{:04.4f}] buffer, channels: {:03} samples: {:03}'.format(time.time() - t0, data.getChannelCount(), data.getSampleCount()))
      for s in range(data.getSampleCount()):
        for c in range(data.getChannelCount()):
          eeg_file.write(' %f' % (data.getSample(c, s)))
        eeg_file.write('\n')
###############################################################################
def test_amplifier(amplifier):
  rates = amplifier.getSamplingRatesAvailable()
  ref_ranges = amplifier.getReferenceRangesAvailable()
  bip_ranges = amplifier.getBipolarRangesAvailable()
  print('amplifier: {}'.format(amplifier_to_id(amplifier)))
  print('  rates....... {}'.format(rates))
  print('  ref ranges.. {}'.format(ref_ranges))
  print('  bip ranges.. {}'.format(bip_ranges))
  print('  channels.... {}'.format(amplifier.getChannelList()))

  #
  # test impedance stream
  #
  try:
    test_impedance(amplifier)
  except Exception as e:
    print('stream error: {}'.format(e))

  #
  # test eeg stream
  #
  try:
    test_eeg(amplifier)
  except Exception as e:
    print('stream error: {}'.format(e))
###############################################################################
if __name__ == '__main__':
  factory = eego_sdk.factory()

  v = factory.getVersion()
  print('version: {}.{}.{}.{}'.format(v.major, v.minor, v.micro, v.build))

  amplifiers=factory.getAmplifiers()
  cascaded={}
  for amplifier in amplifiers:
    try:
      test_amplifier(amplifier)

      # add to cascaded dictionary
      if amplifier.getType() not in cascaded:
        cascaded[amplifier.getType()]=[]
      cascaded[amplifier.getType()].append(amplifier)
    except Exception as e:
      print('amplifier({}) error: {}'.format(amplifier_to_id(amplifier), e))

  for key in cascaded:
    n=len(cascaded[key])
    print('cascaded({}) has {} amplifiers'.format(key, n))
    try:
      if n==2 or n==4:
        test_amplifier(factory.createCascadedAmplifier(cascaded[key]))
    except Exception as e:
      print('cascading({}) error: {}'.format(key, e))
